package main

import "github.com/gorilla/mux"

func dynamicRoutes(router *mux.Router) {

	router.HandleFunc("/target", TrackAndLoad)

	router.HandleFunc("/search", SearchForFeed)

	router.HandleFunc("/live", fetchAndProcessRss)

	router.HandleFunc("/news", NewsFeed)

	router.HandleFunc("/post", PostFeed)

	router.HandleFunc("/", AllFeeds)
}
