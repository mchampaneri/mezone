package main

import (
	"github.com/asaskevich/govalidator"
	"github.com/asdine/storm"
	"github.com/fatih/color"
)

// Db is storm db handle
var Db *storm.DB

func main() {

	//Loading the Config
	loadAppConfig()

	Db, _ = storm.Open(Config.Database.DatabaseName)

	govalidator.SetFieldsRequiredByDefault(false)
	DefaultLogger = Log{}
	// Wiper uses the BigCache as its built in cache service
	// provider . Initializing the  Cache Singleton
	//
	BigCache, _ = initCache()

	// Loading the Routers for the web and api on their
	// mentioned ports
	//
	RegisterWebRoutes()
}
