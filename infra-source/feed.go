package main

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"io/ioutil"

	"github.com/asdine/storm"
	"github.com/asdine/storm/q"

	"github.com/fatih/color"
	strip "github.com/grokify/html-strip-tags-go"
	"github.com/mmcdole/gofeed"
)

const News = 1
const Post = 2

func fetchAndProcessRss(w http.ResponseWriter, r *http.Request) {
	color.Green("Parsing the feed ")
	fp := gofeed.NewParser()

	tutlinks := []string{"https://linxlabs.com/category/golang/feed", "https://golangbot.com/feed",
		"https://golangcode.com/index.xml", "https://mariadesouza.com/category/golang/feed",
		"https://trinhhieu668.wordpress.com/category/big-data/golang/feed",
		"https://www.mchampaneri.in/feeds/posts/default"}

	var tutfeeds []Feed
	for _, link := range tutlinks {
		feed, err := fp.ParseURL(link)

		if err != nil {
			fmt.Println(err.Error(), " during the parsing", "for", link)
			continue
		}

		if len(feed.Items) > 0 {
			for _, item := range feed.Items {

				recordErr := Db.One("Link", item.Link, nil)
				if recordErr.Error() == "not found" {

					client := http.Client{}

					resp, err := client.Get(fmt.Sprint("https://adbilty.me/api?api=aa0248634e8d260077717bd984b7ffe38ae2f2c1&",
						"url=", item.Link, "&format=text"))
					if err != nil {
						DefaultLogger.Error("error during fetching the short url" + err.Error())
						color.Red("error occured ", err.Error())
						break
					} else {
						responseReader, resperr := ioutil.ReadAll(resp.Body)
						resp.Body.Close()
						if resperr != nil {
							color.Red("error on reading the resposne", resperr.Error())
							break
						}
						color.Green(string(responseReader))

						newFeed := Feed{
							HelpLink: string(responseReader),
							Link:     item.Link,
							Type:     Post,
							Title:    item.Title,
							Content:  strip.StripTags(item.Content), //
							Hit:      0,
						}
						if item.UpdatedParsed != nil {
							newFeed.UpdatedAt = *item.UpdatedParsed
						}
						if item.Author != nil {
							newFeed.Author = item.Author.Name
						}
						if len(item.Categories) > 0 {
							newFeed.Labels = item.Categories
						}
						errsave := Db.Save(&newFeed)
						if errsave != nil {
							color.Red(errsave.Error(), " during saving content", newFeed.Link)
							continue
						} else {
							tutfeeds = append(tutfeeds, newFeed)
						}
					}
				}
			}
		}

	}

	newslinks := []string{"http://goz.hexacosa.net/feed", "http://sdet.us/category/golang/feed",
		"https://golang.ch/feed"}

	var newsfeeds []Feed
	for _, link := range newslinks {
		feed, err := fp.ParseURL(link)

		if err != nil {
			fmt.Println(err.Error(), " during the parsing", "for", link)
			continue
		}

		if len(feed.Items) > 0 {
			for _, item := range feed.Items {

				recordErr := Db.One("Link", item.Link, nil)
				if recordErr.Error() == "not found" {

					client := http.Client{}
					resp, err := client.Get(fmt.Sprint("https://adbilty.me/api?api=aa0248634e8d260077717bd984b7ffe38ae2f2c1&",
						"url=", item.Link, "&format=text"))
					if err != nil {
						DefaultLogger.Error("error during fetching the short url" + err.Error())
						color.Red("error occured ", err.Error())
						break
					} else {
						responseReader, resperr := ioutil.ReadAll(resp.Body)
						resp.Body.Close()
						if resperr != nil {
							color.Red("error on reading the resposne", resperr.Error())
							break
						}
						color.Green(string(responseReader))

						newFeed := Feed{
							HelpLink: string(responseReader),
							Link:     item.Link,
							Type:     News,
							Title:    item.Title,
							Content:  strip.StripTags(item.Content),
							Hit:      0,
						}

						if item.UpdatedParsed != nil {
							newFeed.UpdatedAt = *item.UpdatedParsed
						}
						if item.Author != nil {
							newFeed.Author = item.Author.Name
						}
						if len(item.Categories) > 0 {
							newFeed.Labels = item.Categories
						}
						err := Db.Save(&newFeed)
						if err != nil {
							color.Red(err.Error(), " during saving content", newFeed.Link)
							continue
						} else {
							newsfeeds = append(newsfeeds, newFeed)
						}
					}
				}
			}
		}
	}

	fmt.Fprintln(w, " database has been updated succesfully")
}

type Feed struct {
	HelpLink  string   `storm:unique`
	Hit       uint64   `storm:"index"`
	Link      string   `storm:"id"`
	Title     string   `storm:"index"`
	Author    string   `storm:"index"`
	Labels    []string `storm:"index"`
	Type      int      `storm:"index"`
	Content   string
	UpdatedAt time.Time `storm:"index"`
}

func NewsFeed(w http.ResponseWriter, r *http.Request) {
	var localFeeds []Feed

	Db.Find("Type", News, &localFeeds)
	data := make(map[string]interface{})
	// fmt.Println(localFeeds)
	data["feed"] = localFeeds
	data["News"] = "News"
	View(w, r, data, "stored.html")

}

func TrackAndLoad(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fmt.Println("Track and load called")
	r.FormValue("t")
	http.Redirect(w, r, r.FormValue("t"), 301)
	if r.FormValue("t") != "" {
		var TrackHit Feed
		Db.One("Link", r.FormValue("t"), &TrackHit)
		TrackHit.Hit = TrackHit.Hit + 1
		Db.Save(&TrackHit)
		fmt.Println("redicting to after save ", r.FormValue("t"))
	}

}

func PostFeed(w http.ResponseWriter, r *http.Request) {
	var localFeeds []Feed

	Db.Find("Type", Post, &localFeeds)
	data := make(map[string]interface{})
	// fmt.Println(localFeeds)
	data["head"] = "Post"
	data["feed"] = localFeeds
	View(w, r, data, "stored.html")

}

func AllFeeds(w http.ResponseWriter, r *http.Request) {
	var localFeeds []Feed

	Db.AllByIndex("UpdatedAt", &localFeeds, storm.Limit(10), storm.Reverse())

	data := make(map[string]interface{})
	// fmt.Println(localFeeds)
	data["feed"] = localFeeds
	data["head"] = r.FormValue("s")
	View(w, r, data, "stored.html")
}

func SearchForFeed(w http.ResponseWriter, r *http.Request) {

	feedMap := make(map[string]interface{})

	// var Feeds []Feed
	r.ParseForm()
	words := strings.Split(r.FormValue("s"), " ")

	for _, word := range words {
		var localFeeds []Feed
		Db.Select(q.Re("Title", fmt.Sprint("^(?i)[a-zA-Z0-9 ]*\\b", word, "\\b[a-zA-Z0-9 ]*"))).Find(&localFeeds)
		for _, localFeed := range localFeeds {
			feedMap[localFeed.Link] = localFeed
		}
		// Feeds = append(Feeds, localFeeds...)
		color.Yellow("searching for", word)
	}

	// Db.Find("Title", r.FormValue("s"), &localFeeds)
	data := make(map[string]interface{})
	// fmt.Println(localFeeds)
	data["feed"] = feedMap
	data["query"] = r.FormValue("s")
	View(w, r, data, "stored.html")
}
